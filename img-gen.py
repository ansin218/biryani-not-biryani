from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Activation, Dropout, Flatten, Dense
from time import time
import os
import tensorflow as tf

start_time = time()

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

img_width, img_height = 150, 150

train_data_dir = 'dataset/training'
validation_data_dir = 'dataset/validation'

nb_train_samples = 500
nb_validation_samples = 250
nb_epoch = 2

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape = (img_width, img_height, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size = (2,2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size = (2,2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size = (2,2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss = 'binary_crossentropy', optimizer = 'rmsprop', metrics = ['accuracy'])

train_datagen = ImageDataGenerator(rescale = 1./255, shear_range = 0.2, zoom_range = 0.2, horizontal_flip = True)

validation_datagen = ImageDataGenerator(rescale = 1./255)

train_generator = train_datagen.flow_from_directory(
	train_data_dir,
	target_size = (img_width, img_height),
	batch_size = 32,
	class_mode = 'binary')

validation_generator = validation_datagen.flow_from_directory(
	validation_data_dir,
	target_size = (img_width, img_height),
	batch_size = 32,
	class_mode = 'binary')

model.fit_generator(
	train_generator,
	steps_per_epoch = nb_train_samples,
	epochs = nb_epoch,
	validation_data = validation_generator,
	validation_steps = nb_validation_samples)

model.save_weights('biryani_1.h5')

end_time = time()
time_taken = end_time - start_time

print('Total time taken in seconds: ', time_taken)
