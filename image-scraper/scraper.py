#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
from time import time
from fake_useragent import UserAgent
from urllib.request import urlopen, Request, urlretrieve
import urllib

start_time = time()

print('Scraping Not Biryani Images.........')

biryaniRootLink = 'https://www.google.co.in/search?q=shawarma&source=lnms&tbm=isch'
ua1 = UserAgent()
randomHeader = {'User-Agent':str(ua1.random)}
response = urlopen(Request(biryaniRootLink, headers = randomHeader))
soup = BeautifulSoup(response, 'html.parser')

for i in range(50):
    img_info = soup.find_all("div", {"class": "rg_meta"})[i].get_text()
    print('\n')
    imgLink = img_info.split('\"ou\":\"', 1)
    imgLink = imgLink[1].split('\"', 1)
    imgLink = imgLink[0]
    print("Image Link: ", imgLink)
    if '.jpg' in imgLink:
        imgExt = '.jpg'
    elif '.png' in imgLink:
        imgExt = '.png'
    elif '.gif' in imgLink:
        imgExt = '.gif'
    imgName = ' ' + str(i + 950) + imgExt
    try:
        urllib.request.urlretrieve(imgLink, imgName)
    except:
        print('Image Not Found')

end_time = time()
time_taken = end_time - start_time

print('Total time taken in seconds: ', time_taken)
